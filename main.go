package main

import (
	"github.com/sirupsen/logrus"

	"loads/cmd"
	"loads/internal/config"
)

func main() {
	if err := config.Init(); err != nil {
		logrus.WithError(err).Fatal("failed to init config")
	}

	if config.LogPretty() {
		logrus.SetFormatter(&logrus.TextFormatter{
			ForceColors: true,
		})
	}

	level, err := logrus.ParseLevel(config.LogLevel())
	if err != nil {
		logrus.SetLevel(logrus.InfoLevel)
		logrus.Warnf("failed to parse log level: %s", config.LogLevel())
	} else {
		logrus.SetLevel(level)
	}

	if err := cmd.Execute(); err != nil {
		logrus.Fatal(err)
	}
}
