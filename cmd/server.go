package cmd

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"

	"loads/internal/config"
	"loads/internal/db"
	apiV1 "loads/internal/server/v1"
)

const shutdownTimeout = 10 * time.Second

var serverCmd = &cobra.Command{
	Use:   "server",
	Short: "Run API Server",
	Run:   runServer,
}

func runServer(_ *cobra.Command, _ []string) {
	dbClient, err := db.New(config.DataPath())
	if err != nil {
		logrus.WithError(err).Fatal("failed to create DB client")
	}

	handlerV1 := apiV1.New(dbClient)

	e := echo.New()

	e.HideBanner = true
	e.HidePort = true

	e.Use(middleware.Recover())

	v1 := e.Group("/v1")

	v1.GET("/loads", handlerV1.GetLoadsList)

	// start server
	go func() {
		if err := e.Start(fmt.Sprintf(":%d", config.Port())); err != nil {
			logrus.WithError(err).Fatal("failed to start the server")
		}
	}()

	logrus.Infof("server started at port :%d", config.Port())

	// Wait for interrupt signal to gracefully shutdown the server with
	// a timeout of 10 seconds.
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM, os.Interrupt)
	<-quit

	ctx, cancel := context.WithTimeout(context.Background(), shutdownTimeout)

	defer cancel()

	if err := e.Shutdown(ctx); err != nil {
		logrus.WithError(err).Fatal("failed to shutdown the server")
	}
}
