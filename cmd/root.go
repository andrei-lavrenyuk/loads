package cmd

import "github.com/spf13/cobra"

var rootCmd = &cobra.Command{
	Run: func(c *cobra.Command, _ []string) {
		_ = c.Help()
	},
}

// Execute run the root command
func Execute() error {
	rootCmd.AddCommand(serverCmd)

	return rootCmd.Execute()
}
