package models

import "time"

type Driver struct {
	Name string `json:"name"`
}

type Stop struct {
	StartDate *time.Time `json:"start_date"`
	EndDate   *time.Time `json:"end_date"`
}

type Location struct {
	City     string    `json:"city"`
	State    string    `json:"state"`
	Location *Location `json:"location"`
}

type Load struct {
	Number        float64 `json:"load_number"`
	Distance      float64 `json:"distance"`
	Rate          float64 `json:"rate"`
	Weight        float64 `json:"weight"`
	Status        string  `json:"status"`
	Driver        *Driver `json:"driver"`
	TruckNumber   float64 `json:"truck_number"`
	TrailerNumber float64 `json:"trailer_number"`
	Stops         []*Stop `json:"stops"`
}

//easyjson:json
type LoadsList []*Load
