// Code generated by easyjson for marshaling/unmarshaling. DO NOT EDIT.

package models

import (
	json "encoding/json"
	easyjson "github.com/mailru/easyjson"
	jlexer "github.com/mailru/easyjson/jlexer"
	jwriter "github.com/mailru/easyjson/jwriter"
	time "time"
)

// suppress unused package warning
var (
	_ *json.RawMessage
	_ *jlexer.Lexer
	_ *jwriter.Writer
	_ easyjson.Marshaler
)

func easyjsonD2b7633eDecodeLoadsInternalModels(in *jlexer.Lexer, out *Stop) {
	isTopLevel := in.IsStart()
	if in.IsNull() {
		if isTopLevel {
			in.Consumed()
		}
		in.Skip()
		return
	}
	in.Delim('{')
	for !in.IsDelim('}') {
		key := in.UnsafeFieldName(false)
		in.WantColon()
		if in.IsNull() {
			in.Skip()
			in.WantComma()
			continue
		}
		switch key {
		case "start_date":
			if in.IsNull() {
				in.Skip()
				out.StartDate = nil
			} else {
				if out.StartDate == nil {
					out.StartDate = new(time.Time)
				}
				if data := in.Raw(); in.Ok() {
					in.AddError((*out.StartDate).UnmarshalJSON(data))
				}
			}
		case "end_date":
			if in.IsNull() {
				in.Skip()
				out.EndDate = nil
			} else {
				if out.EndDate == nil {
					out.EndDate = new(time.Time)
				}
				if data := in.Raw(); in.Ok() {
					in.AddError((*out.EndDate).UnmarshalJSON(data))
				}
			}
		default:
			in.SkipRecursive()
		}
		in.WantComma()
	}
	in.Delim('}')
	if isTopLevel {
		in.Consumed()
	}
}
func easyjsonD2b7633eEncodeLoadsInternalModels(out *jwriter.Writer, in Stop) {
	out.RawByte('{')
	first := true
	_ = first
	{
		const prefix string = ",\"start_date\":"
		out.RawString(prefix[1:])
		if in.StartDate == nil {
			out.RawString("null")
		} else {
			out.Raw((*in.StartDate).MarshalJSON())
		}
	}
	{
		const prefix string = ",\"end_date\":"
		out.RawString(prefix)
		if in.EndDate == nil {
			out.RawString("null")
		} else {
			out.Raw((*in.EndDate).MarshalJSON())
		}
	}
	out.RawByte('}')
}

// MarshalJSON supports json.Marshaler interface
func (v Stop) MarshalJSON() ([]byte, error) {
	w := jwriter.Writer{}
	easyjsonD2b7633eEncodeLoadsInternalModels(&w, v)
	return w.Buffer.BuildBytes(), w.Error
}

// MarshalEasyJSON supports easyjson.Marshaler interface
func (v Stop) MarshalEasyJSON(w *jwriter.Writer) {
	easyjsonD2b7633eEncodeLoadsInternalModels(w, v)
}

// UnmarshalJSON supports json.Unmarshaler interface
func (v *Stop) UnmarshalJSON(data []byte) error {
	r := jlexer.Lexer{Data: data}
	easyjsonD2b7633eDecodeLoadsInternalModels(&r, v)
	return r.Error()
}

// UnmarshalEasyJSON supports easyjson.Unmarshaler interface
func (v *Stop) UnmarshalEasyJSON(l *jlexer.Lexer) {
	easyjsonD2b7633eDecodeLoadsInternalModels(l, v)
}
func easyjsonD2b7633eDecodeLoadsInternalModels1(in *jlexer.Lexer, out *Location) {
	isTopLevel := in.IsStart()
	if in.IsNull() {
		if isTopLevel {
			in.Consumed()
		}
		in.Skip()
		return
	}
	in.Delim('{')
	for !in.IsDelim('}') {
		key := in.UnsafeFieldName(false)
		in.WantColon()
		if in.IsNull() {
			in.Skip()
			in.WantComma()
			continue
		}
		switch key {
		case "city":
			out.City = string(in.String())
		case "state":
			out.State = string(in.String())
		case "location":
			if in.IsNull() {
				in.Skip()
				out.Location = nil
			} else {
				if out.Location == nil {
					out.Location = new(Location)
				}
				(*out.Location).UnmarshalEasyJSON(in)
			}
		default:
			in.SkipRecursive()
		}
		in.WantComma()
	}
	in.Delim('}')
	if isTopLevel {
		in.Consumed()
	}
}
func easyjsonD2b7633eEncodeLoadsInternalModels1(out *jwriter.Writer, in Location) {
	out.RawByte('{')
	first := true
	_ = first
	{
		const prefix string = ",\"city\":"
		out.RawString(prefix[1:])
		out.String(string(in.City))
	}
	{
		const prefix string = ",\"state\":"
		out.RawString(prefix)
		out.String(string(in.State))
	}
	{
		const prefix string = ",\"location\":"
		out.RawString(prefix)
		if in.Location == nil {
			out.RawString("null")
		} else {
			(*in.Location).MarshalEasyJSON(out)
		}
	}
	out.RawByte('}')
}

// MarshalJSON supports json.Marshaler interface
func (v Location) MarshalJSON() ([]byte, error) {
	w := jwriter.Writer{}
	easyjsonD2b7633eEncodeLoadsInternalModels1(&w, v)
	return w.Buffer.BuildBytes(), w.Error
}

// MarshalEasyJSON supports easyjson.Marshaler interface
func (v Location) MarshalEasyJSON(w *jwriter.Writer) {
	easyjsonD2b7633eEncodeLoadsInternalModels1(w, v)
}

// UnmarshalJSON supports json.Unmarshaler interface
func (v *Location) UnmarshalJSON(data []byte) error {
	r := jlexer.Lexer{Data: data}
	easyjsonD2b7633eDecodeLoadsInternalModels1(&r, v)
	return r.Error()
}

// UnmarshalEasyJSON supports easyjson.Unmarshaler interface
func (v *Location) UnmarshalEasyJSON(l *jlexer.Lexer) {
	easyjsonD2b7633eDecodeLoadsInternalModels1(l, v)
}
func easyjsonD2b7633eDecodeLoadsInternalModels2(in *jlexer.Lexer, out *LoadsList) {
	isTopLevel := in.IsStart()
	if in.IsNull() {
		in.Skip()
		*out = nil
	} else {
		in.Delim('[')
		if *out == nil {
			if !in.IsDelim(']') {
				*out = make(LoadsList, 0, 8)
			} else {
				*out = LoadsList{}
			}
		} else {
			*out = (*out)[:0]
		}
		for !in.IsDelim(']') {
			var v1 *Load
			if in.IsNull() {
				in.Skip()
				v1 = nil
			} else {
				if v1 == nil {
					v1 = new(Load)
				}
				(*v1).UnmarshalEasyJSON(in)
			}
			*out = append(*out, v1)
			in.WantComma()
		}
		in.Delim(']')
	}
	if isTopLevel {
		in.Consumed()
	}
}
func easyjsonD2b7633eEncodeLoadsInternalModels2(out *jwriter.Writer, in LoadsList) {
	if in == nil && (out.Flags&jwriter.NilSliceAsEmpty) == 0 {
		out.RawString("null")
	} else {
		out.RawByte('[')
		for v2, v3 := range in {
			if v2 > 0 {
				out.RawByte(',')
			}
			if v3 == nil {
				out.RawString("null")
			} else {
				(*v3).MarshalEasyJSON(out)
			}
		}
		out.RawByte(']')
	}
}

// MarshalJSON supports json.Marshaler interface
func (v LoadsList) MarshalJSON() ([]byte, error) {
	w := jwriter.Writer{}
	easyjsonD2b7633eEncodeLoadsInternalModels2(&w, v)
	return w.Buffer.BuildBytes(), w.Error
}

// MarshalEasyJSON supports easyjson.Marshaler interface
func (v LoadsList) MarshalEasyJSON(w *jwriter.Writer) {
	easyjsonD2b7633eEncodeLoadsInternalModels2(w, v)
}

// UnmarshalJSON supports json.Unmarshaler interface
func (v *LoadsList) UnmarshalJSON(data []byte) error {
	r := jlexer.Lexer{Data: data}
	easyjsonD2b7633eDecodeLoadsInternalModels2(&r, v)
	return r.Error()
}

// UnmarshalEasyJSON supports easyjson.Unmarshaler interface
func (v *LoadsList) UnmarshalEasyJSON(l *jlexer.Lexer) {
	easyjsonD2b7633eDecodeLoadsInternalModels2(l, v)
}
func easyjsonD2b7633eDecodeLoadsInternalModels3(in *jlexer.Lexer, out *Load) {
	isTopLevel := in.IsStart()
	if in.IsNull() {
		if isTopLevel {
			in.Consumed()
		}
		in.Skip()
		return
	}
	in.Delim('{')
	for !in.IsDelim('}') {
		key := in.UnsafeFieldName(false)
		in.WantColon()
		if in.IsNull() {
			in.Skip()
			in.WantComma()
			continue
		}
		switch key {
		case "load_number":
			out.Number = float64(in.Float64())
		case "distance":
			out.Distance = float64(in.Float64())
		case "rate":
			out.Rate = float64(in.Float64())
		case "weight":
			out.Weight = float64(in.Float64())
		case "status":
			out.Status = string(in.String())
		case "driver":
			if in.IsNull() {
				in.Skip()
				out.Driver = nil
			} else {
				if out.Driver == nil {
					out.Driver = new(Driver)
				}
				(*out.Driver).UnmarshalEasyJSON(in)
			}
		case "truck_number":
			out.TruckNumber = float64(in.Float64())
		case "trailer_number":
			out.TrailerNumber = float64(in.Float64())
		case "stops":
			if in.IsNull() {
				in.Skip()
				out.Stops = nil
			} else {
				in.Delim('[')
				if out.Stops == nil {
					if !in.IsDelim(']') {
						out.Stops = make([]*Stop, 0, 8)
					} else {
						out.Stops = []*Stop{}
					}
				} else {
					out.Stops = (out.Stops)[:0]
				}
				for !in.IsDelim(']') {
					var v4 *Stop
					if in.IsNull() {
						in.Skip()
						v4 = nil
					} else {
						if v4 == nil {
							v4 = new(Stop)
						}
						(*v4).UnmarshalEasyJSON(in)
					}
					out.Stops = append(out.Stops, v4)
					in.WantComma()
				}
				in.Delim(']')
			}
		default:
			in.SkipRecursive()
		}
		in.WantComma()
	}
	in.Delim('}')
	if isTopLevel {
		in.Consumed()
	}
}
func easyjsonD2b7633eEncodeLoadsInternalModels3(out *jwriter.Writer, in Load) {
	out.RawByte('{')
	first := true
	_ = first
	{
		const prefix string = ",\"load_number\":"
		out.RawString(prefix[1:])
		out.Float64(float64(in.Number))
	}
	{
		const prefix string = ",\"distance\":"
		out.RawString(prefix)
		out.Float64(float64(in.Distance))
	}
	{
		const prefix string = ",\"rate\":"
		out.RawString(prefix)
		out.Float64(float64(in.Rate))
	}
	{
		const prefix string = ",\"weight\":"
		out.RawString(prefix)
		out.Float64(float64(in.Weight))
	}
	{
		const prefix string = ",\"status\":"
		out.RawString(prefix)
		out.String(string(in.Status))
	}
	{
		const prefix string = ",\"driver\":"
		out.RawString(prefix)
		if in.Driver == nil {
			out.RawString("null")
		} else {
			(*in.Driver).MarshalEasyJSON(out)
		}
	}
	{
		const prefix string = ",\"truck_number\":"
		out.RawString(prefix)
		out.Float64(float64(in.TruckNumber))
	}
	{
		const prefix string = ",\"trailer_number\":"
		out.RawString(prefix)
		out.Float64(float64(in.TrailerNumber))
	}
	{
		const prefix string = ",\"stops\":"
		out.RawString(prefix)
		if in.Stops == nil && (out.Flags&jwriter.NilSliceAsEmpty) == 0 {
			out.RawString("null")
		} else {
			out.RawByte('[')
			for v5, v6 := range in.Stops {
				if v5 > 0 {
					out.RawByte(',')
				}
				if v6 == nil {
					out.RawString("null")
				} else {
					(*v6).MarshalEasyJSON(out)
				}
			}
			out.RawByte(']')
		}
	}
	out.RawByte('}')
}

// MarshalJSON supports json.Marshaler interface
func (v Load) MarshalJSON() ([]byte, error) {
	w := jwriter.Writer{}
	easyjsonD2b7633eEncodeLoadsInternalModels3(&w, v)
	return w.Buffer.BuildBytes(), w.Error
}

// MarshalEasyJSON supports easyjson.Marshaler interface
func (v Load) MarshalEasyJSON(w *jwriter.Writer) {
	easyjsonD2b7633eEncodeLoadsInternalModels3(w, v)
}

// UnmarshalJSON supports json.Unmarshaler interface
func (v *Load) UnmarshalJSON(data []byte) error {
	r := jlexer.Lexer{Data: data}
	easyjsonD2b7633eDecodeLoadsInternalModels3(&r, v)
	return r.Error()
}

// UnmarshalEasyJSON supports easyjson.Unmarshaler interface
func (v *Load) UnmarshalEasyJSON(l *jlexer.Lexer) {
	easyjsonD2b7633eDecodeLoadsInternalModels3(l, v)
}
func easyjsonD2b7633eDecodeLoadsInternalModels4(in *jlexer.Lexer, out *Driver) {
	isTopLevel := in.IsStart()
	if in.IsNull() {
		if isTopLevel {
			in.Consumed()
		}
		in.Skip()
		return
	}
	in.Delim('{')
	for !in.IsDelim('}') {
		key := in.UnsafeFieldName(false)
		in.WantColon()
		if in.IsNull() {
			in.Skip()
			in.WantComma()
			continue
		}
		switch key {
		case "name":
			out.Name = string(in.String())
		default:
			in.SkipRecursive()
		}
		in.WantComma()
	}
	in.Delim('}')
	if isTopLevel {
		in.Consumed()
	}
}
func easyjsonD2b7633eEncodeLoadsInternalModels4(out *jwriter.Writer, in Driver) {
	out.RawByte('{')
	first := true
	_ = first
	{
		const prefix string = ",\"name\":"
		out.RawString(prefix[1:])
		out.String(string(in.Name))
	}
	out.RawByte('}')
}

// MarshalJSON supports json.Marshaler interface
func (v Driver) MarshalJSON() ([]byte, error) {
	w := jwriter.Writer{}
	easyjsonD2b7633eEncodeLoadsInternalModels4(&w, v)
	return w.Buffer.BuildBytes(), w.Error
}

// MarshalEasyJSON supports easyjson.Marshaler interface
func (v Driver) MarshalEasyJSON(w *jwriter.Writer) {
	easyjsonD2b7633eEncodeLoadsInternalModels4(w, v)
}

// UnmarshalJSON supports json.Unmarshaler interface
func (v *Driver) UnmarshalJSON(data []byte) error {
	r := jlexer.Lexer{Data: data}
	easyjsonD2b7633eDecodeLoadsInternalModels4(&r, v)
	return r.Error()
}

// UnmarshalEasyJSON supports easyjson.Unmarshaler interface
func (v *Driver) UnmarshalEasyJSON(l *jlexer.Lexer) {
	easyjsonD2b7633eDecodeLoadsInternalModels4(l, v)
}
