package server

type Error struct {
	Message string `json:"message"`
	Error   string `json:"error,omitempty"`
}
