package v1

import (
	"net/http"

	"github.com/labstack/echo/v4"

	"loads/internal/db"
	"loads/internal/server"
)

// GetLoadsList returns list of loads
func (h *Handler) GetLoadsList(c echo.Context) error {
	query := &db.LoadsQuery{}
	if err := c.Bind(query); err != nil {
		return c.JSON(http.StatusInternalServerError, server.Error{
			Message: "Something went wrong",
		})
	}

	loads, err := h.db.FindLoads(query)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, server.Error{
			Message: "Something went wrong",
			Error:   err.Error(),
		})
	}

	return c.JSON(http.StatusOK, loads)
}
