package v1

import (
	"loads/internal/db"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
)

func TestGetLoadsList(t *testing.T) {
	dbClient, err := db.New("../../../data/loads_test.json")
	if err != nil {
		logrus.WithError(err).Fatal("failed to create DB client")
	}

	handlerV1 := New(dbClient)

	// Setup
	e := echo.New()

	req := httptest.NewRequest(http.MethodGet, "/v1/loads", nil)
	req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
	rec := httptest.NewRecorder()

	c := e.NewContext(req, rec)

	assert.Nil(t, handlerV1.GetLoadsList(c))
	assert.Equal(t, http.StatusOK, rec.Code)
}
