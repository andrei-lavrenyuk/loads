package v1

import "loads/internal/db"

// Handler
type Handler struct {
	db *db.DB
}

func New(db *db.DB) *Handler {
	return &Handler{
		db: db,
	}
}
