package config

import (
	"errors"

	"github.com/caarlos0/env/v6"
)

var (
	ErrEmptyDataPath = errors.New("empty data path")
)

type serviceConfig struct {
	Port      int    `env:"PORT" envDefault:"3000"`
	DataPath  string `env:"DATA_PATH"`
	LogLevel  string `env:"LOG_LEVEL" envDefault:"INFO"`
	LogPretty bool   `env:"LOG_PRETTY"`
}

var config = &serviceConfig{}

// Init initialize service config
func Init() error {
	if err := env.Parse(config); err != nil {
		return err
	}

	if config.DataPath == "" {
		return ErrEmptyDataPath
	}

	return nil
}

// Port returns the server port
func Port() int {
	return config.Port
}

// DataPath returns the json data path
func DataPath() string {
	return config.DataPath
}

// LogLevel returns the log level
func LogLevel() string {
	return config.LogLevel
}

// LogPretty returns the log pretty flag
func LogPretty() bool {
	return config.LogPretty
}
