package helpers

// IndexOfFloat64Slice returns the index of element in slice
func IndexOfFloat64Slice(item float64, data []float64) int {
	for k, v := range data {
		if item == v {
			return k
		}
	}

	return -1
}

// InStringSlice check if slice contains string
func InStringSlice(item string, data []string) bool {
	for _, v := range data {
		if item == v {
			return true
		}
	}

	return false
}
