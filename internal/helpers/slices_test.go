package helpers

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestInStringSlice(t *testing.T) {
	data := []string{
		"one",
		"two",
		"three",
	}

	t.Run("InStringSliceSuccess", func(t *testing.T) {
		result := InStringSlice("two", data)

		assert.True(t, result)
	})

	t.Run("InStringSliceFail", func(t *testing.T) {
		result := InStringSlice("zero", data)

		assert.False(t, result)
	})
}
