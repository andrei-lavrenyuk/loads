package db

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestFindLoads(t *testing.T) {
	db, err := New("../../data/loads_test.json")

	assert.Nil(t, err)

	t.Run("ByLoadNumber", func(t *testing.T) {
		query := &LoadsQuery{
			LoadNumber: 143684,
		}

		result, err := db.FindLoads(query)

		assert.Nil(t, err)
		assert.Len(t, result, 1)
		assert.Equal(t, float64(query.LoadNumber), result[0].Number)
	})

	t.Run("ByTruckNumber", func(t *testing.T) {
		query := &LoadsQuery{
			TruckNumber: 351,
		}

		result, err := db.FindLoads(query)

		assert.Nil(t, err)
		assert.Len(t, result, 2)

		for _, load := range result {
			assert.Equal(t, float64(query.TruckNumber), load.TruckNumber)
		}
	})

	t.Run("ByTrailerNumber", func(t *testing.T) {
		query := &LoadsQuery{
			TrailerNumber: 5359,
		}

		result, err := db.FindLoads(query)

		assert.Nil(t, err)
		assert.Len(t, result, 2)

		for _, load := range result {
			assert.Equal(t, float64(query.TrailerNumber), load.TrailerNumber)
		}
	})

	t.Run("ByStatus", func(t *testing.T) {
		query := &LoadsQuery{
			Status: "scheduled",
		}

		result, err := db.FindLoads(query)

		assert.Nil(t, err)
		assert.Len(t, result, 4)

		for _, load := range result {
			assert.Equal(t, query.Status, load.Status)
		}
	})

	t.Run("ByDriverName", func(t *testing.T) {
		query := &LoadsQuery{
			DriverName: "Peter Frances",
		}

		result, err := db.FindLoads(query)

		assert.Nil(t, err)
		assert.Len(t, result, 2)

		for _, load := range result {
			assert.Equal(t, query.DriverName, load.Driver.Name)
		}
	})

	t.Run("Pagination", func(t *testing.T) {
		query := &LoadsQuery{
			Page:    2,
			PerPage: 2,
		}

		result, err := db.FindLoads(query)

		assert.Nil(t, err)
		assert.Len(t, result, 2)

		assert.Equal(t, float64(143684), result[0].Number)
		assert.Equal(t, float64(143683), result[1].Number)
	})

	t.Run("Sorting", func(t *testing.T) {
		t.Run("ByLoadNumberAsc", func(t *testing.T) {
			query := &LoadsQuery{
				SortBy:   "load_number",
				SortType: "asc",
			}

			result, err := db.FindLoads(query)

			assert.Nil(t, err)
			assert.Len(t, result, 10)

			for i := 1; i < len(result); i++ {
				assert.Greater(t, result[i].Number, result[i-1].Number)
			}
		})

		t.Run("ByLoadNumberDesc", func(t *testing.T) {
			query := &LoadsQuery{
				SortBy:   "load_number",
				SortType: "desc",
			}

			result, err := db.FindLoads(query)

			assert.Nil(t, err)
			assert.Len(t, result, 10)

			for i := 1; i < len(result); i++ {
				assert.Less(t, result[i].Number, result[i-1].Number)
			}
		})

		t.Run("ByTruckNumberAsc", func(t *testing.T) {
			query := &LoadsQuery{
				SortBy:   "truck_number",
				SortType: "asc",
			}

			result, err := db.FindLoads(query)

			assert.Nil(t, err)
			assert.Len(t, result, 10)

			for i := 1; i < len(result); i++ {
				assert.GreaterOrEqual(t, result[i].TruckNumber, result[i-1].TruckNumber)
			}
		})

		t.Run("ByTruckNumberDesc", func(t *testing.T) {
			query := &LoadsQuery{
				SortBy:   "truck_number",
				SortType: "desc",
			}

			result, err := db.FindLoads(query)

			assert.Nil(t, err)
			assert.Len(t, result, 10)

			for i := 1; i < len(result); i++ {
				assert.LessOrEqual(t, result[i].TruckNumber, result[i-1].TruckNumber)
			}
		})

		t.Run("ByTrailerNumberAsc", func(t *testing.T) {
			query := &LoadsQuery{
				SortBy:   "trailer_number",
				SortType: "asc",
			}

			result, err := db.FindLoads(query)

			assert.Nil(t, err)
			assert.Len(t, result, 10)

			for i := 1; i < len(result); i++ {
				assert.GreaterOrEqual(t, result[i].TrailerNumber, result[i-1].TrailerNumber)
			}
		})

		t.Run("ByTrailerNumberDesc", func(t *testing.T) {
			query := &LoadsQuery{
				SortBy:   "trailer_number",
				SortType: "desc",
			}

			result, err := db.FindLoads(query)

			assert.Nil(t, err)
			assert.Len(t, result, 10)

			for i := 1; i < len(result); i++ {
				assert.LessOrEqual(t, result[i].TrailerNumber, result[i-1].TrailerNumber)
			}
		})

		t.Run("ByStatusAsc", func(t *testing.T) {
			query := &LoadsQuery{
				SortBy:   "status",
				SortType: "asc",
			}

			result, err := db.FindLoads(query)

			assert.Nil(t, err)
			assert.Len(t, result, 10)

			for i := 1; i < len(result); i++ {
				assert.GreaterOrEqual(t, result[i].Status, result[i-1].Status)
			}
		})

		t.Run("ByStatusDesc", func(t *testing.T) {
			query := &LoadsQuery{
				SortBy:   "status",
				SortType: "desc",
			}

			result, err := db.FindLoads(query)

			assert.Nil(t, err)
			assert.Len(t, result, 10)

			for i := 1; i < len(result); i++ {
				assert.LessOrEqual(t, result[i].Status, result[i-1].Status)
			}
		})

		t.Run("ByDriverNameAsc", func(t *testing.T) {
			query := &LoadsQuery{
				SortBy:   "driver_name",
				SortType: "asc",
			}

			result, err := db.FindLoads(query)

			assert.Nil(t, err)
			assert.Len(t, result, 10)

			for i := 1; i < len(result); i++ {
				assert.GreaterOrEqual(t, result[i].Driver.Name, result[i-1].Driver.Name)
			}
		})

		t.Run("ByDriverNameDesc", func(t *testing.T) {
			query := &LoadsQuery{
				SortBy:   "driver_name",
				SortType: "desc",
			}

			result, err := db.FindLoads(query)

			assert.Nil(t, err)
			assert.Len(t, result, 10)

			for i := 1; i < len(result); i++ {
				assert.LessOrEqual(t, result[i].Driver.Name, result[i-1].Driver.Name)
			}
		})
	})
}
