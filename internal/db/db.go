package db

import (
	"encoding/json"
	"io/ioutil"
	"strconv"

	"github.com/sirupsen/logrus"
	"github.com/thedevsaddam/gojsonq/v2"

	"loads/internal/helpers"
	"loads/internal/models"
)

// DB database client
type DB struct {
	loads *gojsonq.JSONQ
	stops *gojsonq.JSONQ
}

// LoadsQuery describes available fields for filtering and sorting
type LoadsQuery struct {
	// load fields
	LoadNumber    int    `query:"load_number"`
	TruckNumber   int    `query:"truck_number"`
	TrailerNumber int    `query:"trailer_number"`
	DriverName    string `query:"driver_name"`
	Status        string `query:"status"`
	// stop fields
	City         string `query:"city"`
	State        string `query:"state"`
	StartDate    string `query:"start_date"`
	MaxStartDate string `query:"max_start_date"`
	MinStartDate string `query:"min_start_date"`
	EndDate      string `query:"end_date"`
	MaxEndDate   string `query:"max_end_date"`
	MinEndDate   string `query:"min_end_date"`
	// sort fields
	SortBy   string `query:"sort_by"`
	SortType string `query:"sort_type"`
	// pagination
	Page    int `query:"page"`
	PerPage int `query:"per_page"`
}

var (
	availableLoadSortFields = []string{
		"load_number",
		"truck_number",
		"trailer_number",
		"driver_name",
		"status",
	}

	availableStopSortFields = []string{
		"city",
		"state",
		"start_date",
		"end_date",
	}
)

// New creates new DB client
func New(dataPath string) (*DB, error) {
	byteValue, err := ioutil.ReadFile(dataPath)
	if err != nil {
		return nil, err
	}

	var (
		loadsData []map[string]interface{}
		stopsData []map[string]interface{}
	)

	err = json.Unmarshal(byteValue, &loadsData)
	if err != nil {
		return nil, err
	}

	fieldsToConvert := []string{"load_number", "truck_number", "trailer_number"}

	for _, item := range loadsData {
		for _, fieldName := range fieldsToConvert {
			fieldValue, ok := item[fieldName]
			if !ok {
				logrus.Errorf("field with name '%s' doesn't exist", fieldName)
				continue
			}

			fieldValueStr, ok := fieldValue.(string)
			if !ok {
				logrus.Errorf("field with name '%s' is not a string", fieldName)
				continue
			}

			value, err := strconv.Atoi(fieldValueStr)
			if err != nil {
				logrus.Error(err)
			} else {
				item[fieldName] = value
			}
		}

		if stops, ok := item["stops"]; ok {
			for _, stopItem := range stops.([]interface{}) {
				if stop, ok := stopItem.(map[string]interface{}); ok {
					stopCopy := make(map[string]interface{})

					// Copy from the original map to the target map
					for key, value := range stop {
						stopCopy[key] = value
					}

					stopCopy["load_number"] = item["load_number"]
					stopsData = append(stopsData, stopCopy)
				}
			}
		}
	}

	loadsJSON, err := json.Marshal(loadsData)
	if err != nil {
		return nil, err
	}

	stopsJSON, err := json.Marshal(stopsData)
	if err != nil {
		return nil, err
	}

	loads := gojsonq.New().FromString(string(loadsJSON))
	stops := gojsonq.New().FromString(string(stopsJSON))

	return &DB{
		loads: loads,
		stops: stops,
	}, nil
}

// FindLoads returns list of loads
func (d *DB) FindLoads(query *LoadsQuery) ([]*models.Load, error) {
	if query == nil {
		// default query
		query = &LoadsQuery{}
	}

	if query.SortBy == "" {
		query.SortBy = "load_number"
	}

	if query.SortType == "" || (query.SortType != "asc" && query.SortType != "desc") {
		query.SortType = "desc"
	}

	if query.Page <= 0 {
		query.Page = 1
	}

	if query.PerPage <= 0 {
		query.PerPage = 10
	}

	var (
		loadNumbers []float64
		err         error
	)

	sortByStops := isSortByStops(query.SortBy)

	loadsJQ := d.loads.Reset()

	// nolint:nestif
	if query.LoadNumber != 0 {
		loadsJQ = loadsJQ.WhereEqual("load_number", query.LoadNumber)
	} else {
		if query.TruckNumber != 0 {
			loadsJQ = loadsJQ.WhereEqual("truck_number", query.TruckNumber)
		}

		if query.TrailerNumber != 0 {
			loadsJQ = loadsJQ.WhereEqual("trailer_number", query.TrailerNumber)
		}

		if query.DriverName != "" {
			loadsJQ = loadsJQ.WhereEqual("driver.name", query.DriverName)
		}

		if query.Status != "" {
			loadsJQ = loadsJQ.WhereEqual("status", query.Status)
		}

		if !sortByStops && helpers.InStringSlice(query.SortBy, availableLoadSortFields) {
			if query.SortBy == "driver_name" {
				query.SortBy = "driver.name"
			}
			loadsJQ = loadsJQ.SortBy(query.SortBy, query.SortType)
		}

		loadNumbers, err = d.findByStops(query)
		if err != nil {
			return nil, err
		}

		if len(loadNumbers) > 0 {
			loadsJQ = loadsJQ.WhereIn("load_number", loadNumbers)
		}
	}

	result := loadsJQ.
		Offset((query.Page - 1) * query.PerPage).
		Limit(query.PerPage).
		Get()

	loads := models.LoadsList{}

	data, err := json.Marshal(result)
	if err != nil {
		return nil, err
	}

	if err := loads.UnmarshalJSON(data); err != nil {
		logrus.Error(err)
	}

	// sort
	// nolint:nestif
	if len(loadNumbers) > 1 && sortByStops {
		for i := 0; i < len(loads); i++ {
			index := helpers.IndexOfFloat64Slice(loads[i].Number, loadNumbers)
			if index > -1 {
				loads[i], loads[index] = loads[index], loads[i]
			}
		}
	}

	return loads, nil
}

func (d *DB) findByStops(lq *LoadsQuery) ([]float64, error) {
	query := map[string]interface{}{}

	if lq == nil {
		return nil, nil
	}

	if lq.City != "" {
		query["location.city"] = lq.City
	}

	if lq.State != "" {
		query["location.state"] = lq.State
	}

	if lq.StartDate != "" {
		query["start_date"] = lq.StartDate
	} else {
		if lq.MaxStartDate != "" {
			query["max_start_date"] = lq.MaxStartDate
		}

		if lq.MinStartDate != "" {
			query["min_start_date"] = lq.MinStartDate
		}
	}

	if lq.EndDate != "" {
		query["end_date"] = lq.EndDate
	} else {
		if lq.MaxEndDate != "" {
			query["max_end_date"] = lq.MaxEndDate
		}

		if lq.MinStartDate != "" {
			query["min_end_date"] = lq.MinEndDate
		}
	}

	if len(query) == 0 {
		return nil, nil
	}

	stopsJQ := d.stops.Reset()

	for field, value := range query {
		switch field {
		case "max_start_date", "max_end_date":
			stopsJQ = stopsJQ.Where(field, "<=", value)
		case "min_start_date", "min_end_date":
			stopsJQ = stopsJQ.Where(field, "=>", value)
		default:
			stopsJQ = stopsJQ.WhereEqual(field, value)
		}
	}

	if isSortByStops(lq.SortBy) {
		stopsJQ = stopsJQ.SortBy(lq.SortBy, lq.SortType)
	}

	jqResult, err := stopsJQ.PluckR("load_number")
	if err != nil {
		return nil, err
	}

	result, err := jqResult.Float64Slice()
	if err != nil {
		return nil, err
	}

	return result, nil
}

func isSortByStops(param string) bool {
	return helpers.InStringSlice(param, availableStopSortFields)
}
