module loads

go 1.14

require (
	github.com/caarlos0/env/v6 v6.4.0
	github.com/kr/text v0.2.0 // indirect
	github.com/labstack/echo/v4 v4.1.17
	github.com/mailru/easyjson v0.7.6
	github.com/mattn/go-colorable v0.1.8 // indirect
	github.com/niemeyer/pretty v0.0.0-20200227124842-a10e7caefd8e // indirect
	github.com/sirupsen/logrus v1.7.0
	github.com/spf13/cobra v1.1.1
	github.com/stretchr/testify v1.6.1
	github.com/thedevsaddam/gojsonq/v2 v2.5.2
	golang.org/x/sys v0.0.0-20201009025420-dfb3f7c4e634 // indirect
	gopkg.in/check.v1 v1.0.0-20200227125254-8fa46927fb4f // indirect
)
