.PHONY: build clean easyjson dev-deps serve run lint test help

.DEFAULT_GOAL := help

APP := loads

build: easyjson ## Build app
	@mkdir -p build
	go build -o build/$(APP)

clean: ## Cleaning up workspace
	@echo "cleaning up workspace"
	@rm -rf build/*

easyjson: ## Generate easyjson code
	easyjson -all internal/models

run: ## Run app
	modd -f modd.conf

lint: ## Lint
	golangci-lint run

tests: ## Run tests
	go test -v ./...

dev-deps: ## Install development dependencies
	go install github.com/joho/godotenv/v6
	go install github.com/cortesi/modd/cmd/modd
	go install github.com/golangci/golangci-lint/cmd/golangci-lint
	go install github.com/mailru/easyjson/...

help: ## Display callable targets
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
